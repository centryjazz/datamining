/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tampil;

import datamining.DataClass.MainDataModel;
import datamining.DataClass.SuportCountModel;
import java.io.DataInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Rikan
 */
public class DataYangDiMining extends javax.swing.JFrame {

    koneksi dbsetting;
    String driver, database, user, pass;
    Object tabel;
    private ArrayList<MainDataModel> dataStream = new ArrayList<>();
    private ArrayList<MainDataModel> dataSuportCount = new ArrayList<>();
    private ArrayList<MainDataModel> dataClean = new ArrayList<>();
    private int jasaUSP;
    private int kblUSP;
    private int tambahUSP;
    private int cicilanUSP;
    private int sisaUSP;
    private int jasaKKH;
    private int kblKKH;
    private int tambahKKH;
    private int cicilanKKH;
    private int sisaKKH;
    private boolean dataStatus = false;

    /**
     * Creates new form datayangdimining
     */
    public DataYangDiMining() {
        initComponents();
        setLocationRelativeTo(this);

        dbsetting = new koneksi();
        driver = dbsetting.SettingPanel("DBDriver");
        database = dbsetting.SettingPanel("DBDatabase");
        user = dbsetting.SettingPanel("DBUsername");
        pass = dbsetting.SettingPanel("DBPassword");

        table_Tampil.setModel(tableModel);
//        init data
        MainDataModel mod = new MainDataModel();
        mod.setIdAnggota(0);
        mod.setIdKKHUSP(0);
        mod.setJasaUSP(0);
        mod.setKblUSP(0);
        mod.setTambahUSP(0);
        mod.setCicilanUSP(0);
        mod.setSisaUSP(0);
        mod.setJasaKKH(0);
        mod.setKblKKH(0);
        mod.setTambahKKH(0);
        mod.setCicilanKKH(0);
        mod.setSisaKKH(0);
        dataSuportCount.add(mod);
//        settableload();
    }

    private void resetValue() {
        jasaUSP = 0;
        kblUSP = 0;
        tambahUSP = 0;
        cicilanUSP = 0;
        sisaUSP = 0;
        jasaKKH = 0;
        kblKKH = 0;
        tambahKKH = 0;
        cicilanKKH = 0;
        sisaKKH = 0;
    }

    private javax.swing.table.DefaultTableModel tableModel = getDefaultTableModel();

    private javax.swing.table.DefaultTableModel getDefaultTableModel() {
        //membuat judul header
        return new javax.swing.table.DefaultTableModel(
                new Object[][]{},
                new String[]{"id USP dan KKH", "id Anggota", "Jasa USP", "KBL USP", "Tambah USP", "Cicilan USP", "Sisa USP", "Jasa KKH", "KBL KKH", "Tambah KKH", "Cicilan KKH", "Sisa KKH"}
        ) //disable perubahan pada grid
        {
            boolean[] canEdit = new boolean[]{
                false, false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        };
    }

    private void settableload() {
        String atat = "";
        try {
            Class.forName(driver);
            Connection kon = DriverManager.getConnection(database, user, pass);
            Statement stt = kon.createStatement();
            String SQL = "SELECT tagihan_usp.id_USP AS 'id_UPS+KKH',tagihan_usp.id_anggota AS 'id_anggota',tagihan_usp.Jasa AS 'jasa_usp', piutang_usp.Keadaan_Bulan_Lalu AS 'kbl_usp'\n"
                    + "                ,piutang_usp.Tambah AS 'tambah_usp',piutang_usp.Cicilan AS 'cicilan_usp',piutang_usp.Sisa AS 'sisa_usp',\n"
                    + "                tagihan_kkh.Jasa AS 'jasa_kkh', piutang_kkh.Keadaan_Bulan_Lalu AS 'kbl_kkh',piutang_kkh.Tambah AS 'tambah_kkh',\n"
                    + "                piutang_kkh.Cicilan AS 'cicilan_kkh',piutang_kkh.Sisa AS 'sisa_kkh' \n"
                    + "                FROM tagihan_usp LEFT JOIN tagihan_kkh ON tagihan_usp.id_USP = tagihan_kkh.id_KKH \n"
                    + "                INNER JOIN  piutang_kkh ON piutang_kkh.id_KKH = tagihan_kkh.id_KKH\n"
                    + "                INNER JOIN  piutang_usp ON piutang_usp.id_usp = tagihan_usp.id_USP\n"
                    + "	        ORDER BY tagihan_usp.id_anggota, tagihan_usp.Tanggal ASC";
            ResultSet res = stt.executeQuery(SQL);
            while (res.next()) {

                MainDataModel dat = new MainDataModel();
                dat.setIdKKHUSP(res.getInt(1));
                dat.setIdAnggota(res.getInt(2));
                dat.setJasaUSP(res.getInt(3));
                dat.setKblUSP(res.getInt(4));
                dat.setTambahUSP(res.getInt(5));
                dat.setCicilanUSP(res.getInt(6));
                dat.setSisaUSP(res.getInt(7));
                dat.setJasaKKH(res.getInt(8));
                dat.setKblKKH(res.getInt(9));
                dat.setTambahKKH(res.getInt(10));
                dat.setCicilanKKH(res.getInt(11));
                dat.setSisaKKH(res.getInt(12));
                dataStream.add(dat);
//                System.out.println("Reading Data SQL....");
            }
            res.close();
            stt.close();
            kon.close();
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.INFORMATION_MESSAGE);
            System.exit(0);
        }

        for (int i = 0; i < dataStream.size(); i++) {
            resetValue();
            if (checkData(dataStream.get(i).getIdAnggota()) == -1) {
                MainDataModel mod = new MainDataModel();
                mod.setIdAnggota(dataStream.get(i).getIdAnggota());
                mod.setIdKKHUSP(dataStream.get(i).getIdKKHUSP());
                mod.setJasaUSP(jasaUSP);
                mod.setKblUSP(kblUSP);
                mod.setTambahUSP(tambahUSP);
                mod.setCicilanUSP(cicilanUSP);
                mod.setSisaUSP(sisaUSP);
                mod.setJasaKKH(jasaKKH);
                mod.setKblKKH(kblKKH);
                mod.setTambahKKH(tambahKKH);
                mod.setCicilanKKH(cicilanKKH);
                mod.setSisaKKH(sisaKKH);
                dataSuportCount.add(mod);
                initData(i);
//                System.out.println("PIN -1");
            } else {
                int idx = checkData(dataStream.get(i).getIdAnggota());
                if (idx != -1) {
                    updateData(i, idx);
//                    System.out.println("PIN -2");
                }

            }

        };

        dataSuportCount.remove(0);
        String data[] = new String[12];
        for (int i = 0; i < dataSuportCount.size(); i++) {
            data[0] = Integer.toString(dataSuportCount.get(i).getIdKKHUSP());
            data[1] = Integer.toString(dataSuportCount.get(i).getIdAnggota());
            data[2] = Integer.toString(dataSuportCount.get(i).getJasaUSP());
            data[3] = Integer.toString(dataSuportCount.get(i).getKblUSP());
            data[4] = Integer.toString(dataSuportCount.get(i).getTambahUSP());
            data[5] = Integer.toString(dataSuportCount.get(i).getCicilanUSP());
            data[6] = Integer.toString(dataSuportCount.get(i).getSisaUSP());
            data[7] = Integer.toString(dataSuportCount.get(i).getJasaKKH());
            data[8] = Integer.toString(dataSuportCount.get(i).getKblKKH());
            data[9] = Integer.toString(dataSuportCount.get(i).getTambahKKH());
            data[10] = Integer.toString(dataSuportCount.get(i).getCicilanKKH());
            data[11] = Integer.toString(dataSuportCount.get(i).getSisaKKH());
            tableModel.addRow(data);
        }
    }

    private int checkData(int id) {
        int idx = -1;
        for (int i = 0; i < dataSuportCount.size(); i++) {
            if (id == dataSuportCount.get(i).getIdAnggota()) {
                idx = i;
//                System.out.println("DO Break !!");
                break;
            }
        }
        return idx;
    }

    private void updateData(int idxAwal, int idxTarget) {
        if (dataStream.get(idxAwal).getJasaUSP() != 0) {
            dataSuportCount.get(idxTarget).setJasaUSP(dataSuportCount.get(idxTarget).getJasaUSP() + 1);
        }
        if (dataStream.get(idxAwal).getCicilanUSP() != 0) {
            dataSuportCount.get(idxTarget).setCicilanUSP(dataSuportCount.get(idxTarget).getCicilanUSP() + 1);
        }
        if (dataStream.get(idxAwal).getKblUSP() != 0) {
            dataSuportCount.get(idxTarget).setKblUSP(dataSuportCount.get(idxTarget).getKblUSP() + 1);;
        }
        if (dataStream.get(idxAwal).getSisaUSP() != 0) {
            dataSuportCount.get(idxTarget).setSisaUSP(dataSuportCount.get(idxTarget).getSisaUSP() + 1);;
        }
        if (dataStream.get(idxAwal).getTambahUSP() != 0) {
            dataSuportCount.get(idxTarget).setTambahUSP(dataSuportCount.get(idxTarget).getTambahUSP() + 1);;
        }
        // KKH
        if (dataStream.get(idxAwal).getJasaKKH() != 0) {
            dataSuportCount.get(idxTarget).setJasaKKH(dataSuportCount.get(idxTarget).getJasaKKH() + 1);
        }
        if (dataStream.get(idxAwal).getCicilanKKH() != 0) {
            dataSuportCount.get(idxTarget).setCicilanKKH(dataSuportCount.get(idxTarget).getCicilanKKH() + 1);
        }
        if (dataStream.get(idxAwal).getKblKKH() != 0) {
            dataSuportCount.get(idxTarget).setKblKKH(dataSuportCount.get(idxTarget).getKblKKH() + 1);;
        }
        if (dataStream.get(idxAwal).getSisaKKH() != 0) {
            dataSuportCount.get(idxTarget).setSisaKKH(dataSuportCount.get(idxTarget).getSisaKKH() + 1);;
        }
        if (dataStream.get(idxAwal).getTambahKKH() != 0) {
            dataSuportCount.get(idxTarget).setTambahKKH(dataSuportCount.get(idxTarget).getTambahKKH() + 1);;
        }
    }

    private void initData(int idx) {
        if (dataStream.get(idx).getJasaUSP() != 0) {
            dataSuportCount.get(dataSuportCount.size() - 1).setJasaUSP(dataSuportCount.get(dataSuportCount.size() - 1).getJasaUSP() + 1);
        }
        if (dataStream.get(idx).getCicilanUSP() != 0) {
            dataSuportCount.get(dataSuportCount.size() - 1).setCicilanUSP(dataSuportCount.get(dataSuportCount.size() - 1).getCicilanUSP() + 1);
        }
        if (dataStream.get(idx).getKblUSP() != 0) {
            dataSuportCount.get(dataSuportCount.size() - 1).setKblUSP(dataSuportCount.get(dataSuportCount.size() - 1).getKblUSP() + 1);;
        }
        if (dataStream.get(idx).getSisaUSP() != 0) {
            dataSuportCount.get(dataSuportCount.size() - 1).setSisaUSP(dataSuportCount.get(dataSuportCount.size() - 1).getSisaUSP() + 1);;
        }
        if (dataStream.get(idx).getTambahUSP() != 0) {
            dataSuportCount.get(dataSuportCount.size() - 1).setTambahUSP(dataSuportCount.get(dataSuportCount.size() - 1).getTambahUSP() + 1);;
        }
//                //KKH
        if (dataStream.get(idx).getJasaKKH() != 0) {
            dataSuportCount.get(dataSuportCount.size() - 1).setJasaKKH(dataSuportCount.get(dataSuportCount.size() - 1).getJasaKKH() + 1);
        }
        if (dataStream.get(idx).getCicilanKKH() != 0) {
            dataSuportCount.get(dataSuportCount.size() - 1).setCicilanKKH(dataSuportCount.get(dataSuportCount.size() - 1).getCicilanKKH() + 1);
        }
        if (dataStream.get(idx).getKblKKH() != 0) {
            dataSuportCount.get(dataSuportCount.size() - 1).setKblKKH(dataSuportCount.get(dataSuportCount.size() - 1).getKblKKH() + 1);;
        }
        if (dataStream.get(idx).getSisaKKH() != 0) {
            dataSuportCount.get(dataSuportCount.size() - 1).setSisaKKH(dataSuportCount.get(dataSuportCount.size() - 1).getSisaKKH() + 1);;
        }
        if (dataStream.get(idx).getTambahKKH() != 0) {
            dataSuportCount.get(dataSuportCount.size() - 1).setTambahKKH(dataSuportCount.get(dataSuportCount.size() - 1).getTambahKKH() + 1);;
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        table_Tampil = new javax.swing.JTable();
        btnAction = new javax.swing.JButton();
        lblJudul = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        table_Tampil.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4", "Title 5", "Title 6", "Title 7", "Title 8", "Title 9", "Title 10", "Title 11", "Title 12"
            }
        ));
        jScrollPane1.setViewportView(table_Tampil);

        btnAction.setText("Clean Data");
        btnAction.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActionActionPerformed(evt);
            }
        });

        lblJudul.setFont(new java.awt.Font("Century Gothic", 1, 24)); // NOI18N
        lblJudul.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblJudul.setText("RAW DATA");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 952, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnAction, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblJudul, javax.swing.GroupLayout.PREFERRED_SIZE, 941, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblJudul, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 458, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnAction, javax.swing.GroupLayout.DEFAULT_SIZE, 47, Short.MAX_VALUE)
                .addGap(18, 18, 18))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        settableload();
    }//GEN-LAST:event_formWindowOpened

    private void cleanData() {
        int countZero = 0;
        // check data

        //
        ArrayList<Integer> dataZero = new ArrayList<>();
        for (int i = 0; i < dataSuportCount.size(); i++) {
            //counting zero value
            if (dataSuportCount.get(i).getJasaUSP() == 0) {
                countZero++;

            }
            if (dataSuportCount.get(i).getKblUSP() == 0) {
                countZero++;
            }
            if (dataSuportCount.get(i).getTambahUSP() == 0) {
                countZero++;
            }
            if (dataSuportCount.get(i).getCicilanUSP() == 0) {
                countZero++;
            }
            if (dataSuportCount.get(i).getSisaUSP() == 0) {
                countZero++;
            }
            if (dataSuportCount.get(i).getJasaKKH() == 0) {
                countZero++;
            }
            if (dataSuportCount.get(i).getKblKKH() == 0) {
                countZero++;
            }
            if (dataSuportCount.get(i).getTambahKKH() == 0) {
                countZero++;
            }
            if (dataSuportCount.get(i).getCicilanKKH() == 0) {
                countZero++;
            }
            if (dataSuportCount.get(i).getSisaKKH() == 0) {
                countZero++;
            }

            //check data coutn zero value
            if (countZero == 10) {
                dataZero.add(dataSuportCount.get(i).getIdAnggota());
                countZero = 0;
            } else {
                countZero = 0;
            }
        }
        removeZeroData(dataZero);
    }

    private void removeZeroData(ArrayList<Integer> dataRemove) {

        try {
            int idx = 0;
            Iterator<MainDataModel> it = dataSuportCount.iterator();
            while (it.hasNext()) {
                MainDataModel mainDataModel = it.next();
                if (mainDataModel.getIdAnggota() == dataRemove.get(idx)) {
                    it.remove();
                    idx++;
                }

            }
            tampilCleanDataToTabel();
            this.dataClean = dataSuportCount;
        } catch (Exception e) {
            System.out.println("Error removing Zero Data !!!");
            System.out.println("#~> " + e);
        }

    }

    private void tampilCleanDataToTabel() {
        DefaultTableModel dtm = (DefaultTableModel) table_Tampil.getModel();
        dtm.setRowCount(0);
        String data[] = new String[12];
        for (int i = 0; i < dataSuportCount.size(); i++) {
            data[0] = Integer.toString(dataSuportCount.get(i).getIdKKHUSP());
            data[1] = Integer.toString(dataSuportCount.get(i).getIdAnggota());
            data[2] = Integer.toString(dataSuportCount.get(i).getJasaUSP());
            data[3] = Integer.toString(dataSuportCount.get(i).getKblUSP());
            data[4] = Integer.toString(dataSuportCount.get(i).getTambahUSP());
            data[5] = Integer.toString(dataSuportCount.get(i).getCicilanUSP());
            data[6] = Integer.toString(dataSuportCount.get(i).getSisaUSP());
            data[7] = Integer.toString(dataSuportCount.get(i).getJasaKKH());
            data[8] = Integer.toString(dataSuportCount.get(i).getKblKKH());
            data[9] = Integer.toString(dataSuportCount.get(i).getTambahKKH());
            data[10] = Integer.toString(dataSuportCount.get(i).getCicilanKKH());
            data[11] = Integer.toString(dataSuportCount.get(i).getSisaKKH());
            tableModel.addRow(data);
//            System.out.println("Construct Data....");
        }

    }
    private void btnActionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActionActionPerformed
        if (dataStatus == true) {
            PreProcess preProcess = new PreProcess(dataStream, dataSuportCount, dataClean);
            preProcess.setVisible(true);
            this.dispose();
        } else {
            try {
                cleanData();
                JOptionPane.showMessageDialog(this, "Data Telah Dibersihkan.", "information", JOptionPane.INFORMATION_MESSAGE);
                btnAction.setText("Proccess");
                lblJudul.setText("Clean Data");
                dataStatus = true;
            } catch (Exception e) {
                JOptionPane.showMessageDialog(this, "Gagal Melakukan Proses Pembersihan data.", "Failed", JOptionPane.ERROR_MESSAGE);
                System.out.println(e);
            }
        }


    }//GEN-LAST:event_btnActionActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DataYangDiMining.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DataYangDiMining.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DataYangDiMining.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DataYangDiMining.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new DataYangDiMining().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAction;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblJudul;
    private javax.swing.JTable table_Tampil;
    // End of variables declaration//GEN-END:variables
}
