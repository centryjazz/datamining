/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tampil;

import datamining.DataClass.MainDataModel;
import datamining.DataClass.PriorityTransactionListModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.stream.Collectors;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Centry
 */
public class PreProcess extends javax.swing.JFrame {

    private javax.swing.table.DefaultTableModel tableModel = getDefaultTableModelForTabelPriority();
    private ArrayList<MainDataModel> stream = new ArrayList<>();
    private ArrayList<MainDataModel> dataSuportCount = new ArrayList<>();
    private ArrayList<MainDataModel> dataClean = new ArrayList<>();
    private ArrayList<PriorityTransactionListModel> suportcount = new ArrayList<>();

    public PreProcess(ArrayList<MainDataModel> stream, ArrayList<MainDataModel> dataSuportCount, ArrayList<MainDataModel> dataClean) {
        initComponents();
        tbl_priorityList.setModel(tableModel);
        this.dataSuportCount = dataSuportCount;
        this.dataClean = dataClean;
        this.stream = stream;
        SetPriority();
    }

    private javax.swing.table.DefaultTableModel getDefaultTableModelForTabelPriority() {
        //membuat judul header
        return new javax.swing.table.DefaultTableModel(
                new Object[][]{},
                new String[]{"id Anggota", "Jasa USP", "KBL USP", "Tambah USP", "Cicilan USP", "Sisa USP", "Jasa KKH", "KBL KKH", "Tambah KKH", "Cicilan KKH", "Sisa KKH"}
        ) {
            boolean[] canEdit = new boolean[]{
                false, false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        };
    }

    private void setToTabelProiroty(ArrayList<MainDataModel> dataListPriority) {

    }

    private void getShort(int idx) {
        switch (idx) {
            case 0:
                suportcount.sort(Comparator.comparing(e -> e.getPriority_jasaUSP()));
                break;
            case 1:
                suportcount.sort(Comparator.comparing(e -> e.getPriority_kblUSP()));
                break;
            case 2:
                suportcount.sort(Comparator.comparing(e -> e.getPriority_tambahUSP()));
                break;
            case 3:
                suportcount.sort(Comparator.comparing(e -> e.getPriority_cicilanUSP()));
                break;
            case 4:
                suportcount.sort(Comparator.comparing(e -> e.getPriority_sisaUSP()));
                break;
            case 5:
                suportcount.sort(Comparator.comparing(e -> e.getPriority_jasaKKH()));
                break;
            case 6:
                suportcount.sort(Comparator.comparing(e -> e.getPriority_kblKKH()));
                break;
            case 7:
                suportcount.sort(Comparator.comparing(e -> e.getPriority_tambahKKH()));
                break;
            case 8:
                suportcount.sort(Comparator.comparing(e -> e.getPriority_tambahKKH()));
                break;
            case 9:
                suportcount.sort(Comparator.comparing(e -> e.getPriority_sisaKKH()));

                break;
        }

    }

    private void ShortDataByJasaUSP() {
        Comparator<MainDataModel> comparator = Comparator.comparing(e -> e.getJasaUSP());
        dataSuportCount.sort(comparator.reversed());
        int nextPriority = 1;
        for (int i = 0; i < dataSuportCount.size(); i++) {
            for (int j = 0; j < suportcount.size(); j++) {
//                System.out.println("data Clean id "+dataSuportCount.get(i).getIdAnggota()+" | " +suportcount.get(j).getIdAnggota());
                if (dataSuportCount.get(i).getIdAnggota() == suportcount.get(j).getIdAnggota()) {
                    suportcount.get(j).setPriority_jasaUSP(nextPriority);
                    nextPriority++;
                }
            }
        }

    }

    private void ShortDataByKblUSP() {
        Comparator<MainDataModel> comparator = Comparator.comparing(e -> e.getKblUSP());
        dataSuportCount.sort(comparator.reversed());
        int nextPriority = 1;
        for (int i = 0; i < dataSuportCount.size(); i++) {
            for (int j = 0; j < suportcount.size(); j++) {
//                System.out.println("data Clean id "+dataSuportCount.get(i).getIdAnggota()+" | " +suportcount.get(j).getIdAnggota());
                if (dataSuportCount.get(i).getIdAnggota() == suportcount.get(j).getIdAnggota()) {
                    suportcount.get(j).setPriority_kblUSP(nextPriority);
                    nextPriority++;
                }
            }
        }
    }

    private void ShortDataByCicilanUSP() {
        Comparator<MainDataModel> comparator = Comparator.comparing(e -> e.getCicilanUSP());
        dataSuportCount.sort(comparator.reversed());
        int nextPriority = 1;
        for (int i = 0; i < dataSuportCount.size(); i++) {
            for (int j = 0; j < suportcount.size(); j++) {
//                System.out.println("data Clean id "+dataSuportCount.get(i).getIdAnggota()+" | " +suportcount.get(j).getIdAnggota());
                if (dataSuportCount.get(i).getIdAnggota() == suportcount.get(j).getIdAnggota()) {
                    suportcount.get(j).setPriority_cicilanUSP(nextPriority);
                    nextPriority++;
                }
            }
        }
    }

    private void ShortDataBySisaUSP() {
        Comparator<MainDataModel> comparator = Comparator.comparing(e -> e.getSisaUSP());
        dataSuportCount.sort(comparator.reversed());
        int nextPriority = 1;
        for (int i = 0; i < dataSuportCount.size(); i++) {
            for (int j = 0; j < suportcount.size(); j++) {
//                System.out.println("data Clean id "+dataSuportCount.get(i).getIdAnggota()+" | " +suportcount.get(j).getIdAnggota());
                if (dataSuportCount.get(i).getIdAnggota() == suportcount.get(j).getIdAnggota()) {
                    suportcount.get(j).setPriority_sisaUSP(nextPriority);
                    nextPriority++;
                }
            }
        }
    }

    private void ShortDataByTambahUSP() {
        Comparator<MainDataModel> comparator = Comparator.comparing(e -> e.getTambahUSP());
        dataSuportCount.sort(comparator.reversed());
        int nextPriority = 1;
        for (int i = 0; i < dataSuportCount.size(); i++) {
            for (int j = 0; j < suportcount.size(); j++) {
//                System.out.println("data Clean id "+dataSuportCount.get(i).getIdAnggota()+" | " +suportcount.get(j).getIdAnggota());
                if (dataSuportCount.get(i).getIdAnggota() == suportcount.get(j).getIdAnggota()) {
                    suportcount.get(j).setPriority_tambahUSP(nextPriority);
                    nextPriority++;
                }
            }
        }
    }

    private void ShortDataByJasaKKH() {
        Comparator<MainDataModel> comparator = Comparator.comparing(e -> e.getJasaKKH());
        dataSuportCount.sort(comparator.reversed());
        int nextPriority = 1;
        for (int i = 0; i < dataSuportCount.size(); i++) {
            for (int j = 0; j < suportcount.size(); j++) {
//                System.out.println("data Clean id "+dataSuportCount.get(i).getIdAnggota()+" | " +suportcount.get(j).getIdAnggota());
                if (dataSuportCount.get(i).getIdAnggota() == suportcount.get(j).getIdAnggota()) {
                    suportcount.get(j).setPriority_jasaKKH(nextPriority);
                    nextPriority++;
                }
            }
        }

    }

    private void ShortDataByKblKKH() {
        Comparator<MainDataModel> comparator = Comparator.comparing(e -> e.getKblUSP());
        dataSuportCount.sort(comparator.reversed());
        int nextPriority = 1;
        for (int i = 0; i < dataSuportCount.size(); i++) {
            for (int j = 0; j < suportcount.size(); j++) {
//                System.out.println("data Clean id "+dataSuportCount.get(i).getIdAnggota()+" | " +suportcount.get(j).getIdAnggota());
                if (dataSuportCount.get(i).getIdAnggota() == suportcount.get(j).getIdAnggota()) {
                    suportcount.get(j).setPriority_kblKKH(nextPriority);
                    nextPriority++;
                }
            }
        }
    }

    private void ShortDataByCicilanKKH() {
        Comparator<MainDataModel> comparator = Comparator.comparing(e -> e.getCicilanUSP());
        dataSuportCount.sort(comparator.reversed());
        int nextPriority = 1;
        for (int i = 0; i < dataSuportCount.size(); i++) {
            for (int j = 0; j < suportcount.size(); j++) {
//                System.out.println("data Clean id "+dataSuportCount.get(i).getIdAnggota()+" | " +suportcount.get(j).getIdAnggota());
                if (dataSuportCount.get(i).getIdAnggota() == suportcount.get(j).getIdAnggota()) {
                    suportcount.get(j).setPriority_cicilanKKH(nextPriority);
                    nextPriority++;
                }
            }
        }
    }

    private void ShortDataByTambahKKH() {
        Comparator<MainDataModel> comparator = Comparator.comparing(e -> e.getTambahKKH());
        dataSuportCount.sort(comparator.reversed());
        int nextPriority = 1;
        for (int i = 0; i < dataSuportCount.size(); i++) {
            for (int j = 0; j < suportcount.size(); j++) {
//                System.out.println("data Clean id "+dataSuportCount.get(i).getIdAnggota()+" | " +suportcount.get(j).getIdAnggota());
                if (dataSuportCount.get(i).getIdAnggota() == suportcount.get(j).getIdAnggota()) {
                    suportcount.get(j).setPriority_tambahKKH(nextPriority);
                    nextPriority++;
                }
            }
        }
    }

    private void ShortDataBySisaKKH() {
        Comparator<MainDataModel> comparator = Comparator.comparing(e -> e.getSisaUSP());
        dataSuportCount.sort(comparator.reversed());
        int nextPriority = 1;
        for (int i = 0; i < dataSuportCount.size(); i++) {
            for (int j = 0; j < suportcount.size(); j++) {
//                System.out.println("data Clean id "+dataSuportCount.get(i).getIdAnggota()+" | " +suportcount.get(j).getIdAnggota());
                if (dataSuportCount.get(i).getIdAnggota() == suportcount.get(j).getIdAnggota()) {
                    suportcount.get(j).setPriority_sisaKKH(nextPriority);
                    nextPriority++;
                }
            }
        }
    }

    private void SetPriority() {
        for (int i = 0; i < dataSuportCount.size(); i++) {
            PriorityTransactionListModel priorityTransactionListModel = new PriorityTransactionListModel();
            priorityTransactionListModel.setIdAnggota(dataSuportCount.get(i).getIdAnggota());
            priorityTransactionListModel.setIdKKHUSP(dataSuportCount.get(i).getIdKKHUSP());
            priorityTransactionListModel.setJasaUSP(dataSuportCount.get(i).getJasaUSP());
            priorityTransactionListModel.setKblUSP(dataSuportCount.get(i).getKblUSP());
            priorityTransactionListModel.setTambahUSP(dataSuportCount.get(i).getTambahUSP());
            priorityTransactionListModel.setCicilanUSP(dataSuportCount.get(i).getCicilanUSP());
            priorityTransactionListModel.setSisaUSP(dataSuportCount.get(i).getSisaUSP());
            priorityTransactionListModel.setJasaKKH(dataSuportCount.get(i).getJasaKKH());
            priorityTransactionListModel.setKblKKH(dataSuportCount.get(i).getKblKKH());
            priorityTransactionListModel.setTambahKKH(dataSuportCount.get(i).getTambahKKH());
            priorityTransactionListModel.setCicilanKKH(dataSuportCount.get(i).getCicilanKKH());
            priorityTransactionListModel.setSisaKKH(dataSuportCount.get(i).getSisaKKH());
            suportcount.add(priorityTransactionListModel);
        }
        ShortDataByJasaUSP();
        ShortDataByKblUSP();
        ShortDataByCicilanUSP();
        ShortDataBySisaUSP();
        ShortDataByTambahUSP();
        ShortDataByJasaKKH();
        ShortDataByKblKKH();
        ShortDataByCicilanKKH();
        ShortDataBySisaKKH();
        ShortDataByTambahKKH();
        setToTabelPriorityList();
    }

    private void setToTabelPriorityList() {
        DefaultTableModel dtm = (DefaultTableModel) tbl_priorityList.getModel();
        dtm.setRowCount(0);
        String data[] = new String[12];
        for (int i = 0; i < suportcount.size(); i++) {
                data[0] = Integer.toString(suportcount.get(i).getIdAnggota());
                data[1] = Integer.toString(suportcount.get(i).getJasaUSP());
                data[2] = Integer.toString(suportcount.get(i).getKblUSP());
                data[3] = Integer.toString(suportcount.get(i).getTambahUSP());
                data[4] = Integer.toString(suportcount.get(i).getCicilanUSP());
                data[5] = Integer.toString(suportcount.get(i).getSisaUSP());
                data[6] = Integer.toString(suportcount.get(i).getJasaKKH());
                data[7] = Integer.toString(suportcount.get(i).getKblKKH());
                data[8] = Integer.toString(suportcount.get(i).getTambahKKH());
                data[9] = Integer.toString(suportcount.get(i).getCicilanKKH());
                data[10] = Integer.toString(suportcount.get(i).getSisaKKH());
                tableModel.addRow(data);

            if (cmbPriority.isSelected() == true) {
                data[0] = "Priority ~>";
                data[1] = Integer.toString(suportcount.get(i).getPriority_jasaUSP());
                data[2] = Integer.toString(suportcount.get(i).getPriority_kblUSP());
                data[3] = Integer.toString(suportcount.get(i).getPriority_tambahUSP());
                data[4] = Integer.toString(suportcount.get(i).getPriority_cicilanUSP());
                data[5] = Integer.toString(suportcount.get(i).getPriority_sisaUSP());
                data[6] = Integer.toString(suportcount.get(i).getPriority_jasaKKH());
                data[7] = Integer.toString(suportcount.get(i).getPriority_kblKKH());
                data[8] = Integer.toString(suportcount.get(i).getPriority_tambahKKH());
                data[9] = Integer.toString(suportcount.get(i).getPriority_cicilanKKH());
                data[10] = Integer.toString(suportcount.get(i).getPriority_sisaKKH());
                tableModel.addRow(data);
            }

        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_priorityList = new javax.swing.JTable();
        cmbPriority = new javax.swing.JCheckBox();
        cmbShort = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        lblJudul = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jTabbedPane1.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        tbl_priorityList.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        tbl_priorityList.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tbl_priorityList);

        cmbPriority.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        cmbPriority.setSelected(true);
        cmbPriority.setText("Lihat Priority");
        cmbPriority.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbPriorityActionPerformed(evt);
            }
        });

        cmbShort.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        cmbShort.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Jasa USP", "KBL USP", "Tambah USP", "Cicilan USP", "Sisa USP", "Jasa KKH", "KBL KKH", "Tambah KKH", "Cicilan KKH", "Sisa KKH" }));
        cmbShort.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbShortActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        jLabel1.setText("Short By");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cmbShort, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(cmbPriority)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 967, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbPriority)
                    .addComponent(cmbShort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 460, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Priority List", jPanel1);

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 991, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 527, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Transaction Suport Count", jPanel3);

        lblJudul.setFont(new java.awt.Font("Century Gothic", 1, 24)); // NOI18N
        lblJudul.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblJudul.setText("Pre Processing");

        jButton1.setText("Process");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblJudul, javax.swing.GroupLayout.PREFERRED_SIZE, 1010, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(layout.createSequentialGroup()
                .addGap(759, 759, 759)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(lblJudul)
                .addGap(19, 19, 19)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 560, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void cmbShortActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbShortActionPerformed
        getShort(cmbShort.getSelectedIndex());
        setToTabelPriorityList();
    }//GEN-LAST:event_cmbShortActionPerformed

    private void cmbPriorityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbPriorityActionPerformed
        setToTabelPriorityList();
    }//GEN-LAST:event_cmbPriorityActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PreProcess.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PreProcess.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PreProcess.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PreProcess.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
//                new PreProcess().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox cmbPriority;
    private javax.swing.JComboBox<String> cmbShort;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel lblJudul;
    private javax.swing.JTable tbl_priorityList;
    // End of variables declaration//GEN-END:variables
}
