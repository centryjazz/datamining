/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamining.DataClass;

/**
 *
 * @author Centry
 */
public class SuportCountModel {
    private int idAnggota;
    private int suportCount;

    public int getIdAnggota() {
        return idAnggota;
    }

    public void setIdAnggota(int idAnggota) {
        this.idAnggota = idAnggota;
    }

    public int getSuportCount() {
        return suportCount;
    }

    public void setSuportCount(int suportCount) {
        this.suportCount = suportCount;
    }
    
}
