/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamining.DataClass;

/**
 *
 * @author Centry
 */
public class PriorityTransactionListModel extends MainDataModel{

    private int priority_jasaUSP;
    private int priority_kblUSP;
    private int priority_tambahUSP;
    private int priority_cicilanUSP;
    private int priority_sisaUSP;
    private int priority_jasaKKH;
    private int priority_kblKKH;
    private int priority_tambahKKH;
    private int priority_cicilanKKH;
    private int priority_sisaKKH;

    public int getPriority_jasaUSP() {
        return priority_jasaUSP;
    }

    public void setPriority_jasaUSP(int priority_jasaUSP) {
        this.priority_jasaUSP = priority_jasaUSP;
    }

    public int getPriority_kblUSP() {
        return priority_kblUSP;
    }

    public void setPriority_kblUSP(int priority_kblUSP) {
        this.priority_kblUSP = priority_kblUSP;
    }

    public int getPriority_tambahUSP() {
        return priority_tambahUSP;
    }

    public void setPriority_tambahUSP(int priority_tambahUSP) {
        this.priority_tambahUSP = priority_tambahUSP;
    }

    public int getPriority_cicilanUSP() {
        return priority_cicilanUSP;
    }

    public void setPriority_cicilanUSP(int priority_cicilanUSP) {
        this.priority_cicilanUSP = priority_cicilanUSP;
    }

    public int getPriority_sisaUSP() {
        return priority_sisaUSP;
    }

    public void setPriority_sisaUSP(int priority_sisaUSP) {
        this.priority_sisaUSP = priority_sisaUSP;
    }

    public int getPriority_jasaKKH() {
        return priority_jasaKKH;
    }

    public void setPriority_jasaKKH(int priority_jasaKKH) {
        this.priority_jasaKKH = priority_jasaKKH;
    }

    public int getPriority_kblKKH() {
        return priority_kblKKH;
    }

    public void setPriority_kblKKH(int priority_kblKKH) {
        this.priority_kblKKH = priority_kblKKH;
    }

    public int getPriority_tambahKKH() {
        return priority_tambahKKH;
    }

    public void setPriority_tambahKKH(int priority_tambahKKH) {
        this.priority_tambahKKH = priority_tambahKKH;
    }

    public int getPriority_cicilanKKH() {
        return priority_cicilanKKH;
    }

    public void setPriority_cicilanKKH(int priority_cicilanKKH) {
        this.priority_cicilanKKH = priority_cicilanKKH;
    }

    public int getPriority_sisaKKH() {
        return priority_sisaKKH;
    }

    public void setPriority_sisaKKH(int priority_sisaKKH) {
        this.priority_sisaKKH = priority_sisaKKH;
    }
    
    
}
