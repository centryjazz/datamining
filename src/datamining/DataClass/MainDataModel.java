/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamining.DataClass;

/**
 *
 * @author Centry
 */
public class MainDataModel {
   private int idKKHUSP;
   private int idAnggota;
   private int jasaUSP;
   private int kblUSP;
   private int tambahUSP;
   private int cicilanUSP;
   private int sisaUSP;
   private int jasaKKH;
   private int kblKKH;
   private int tambahKKH;
   private int cicilanKKH;
   private int sisaKKH;

    public int getIdKKHUSP() {
        return idKKHUSP;
    }

    public void setIdKKHUSP(int idKKHUSP) {
        this.idKKHUSP = idKKHUSP;
    }

   
    public int getIdAnggota() {
        return idAnggota;
    }

    public void setIdAnggota(int idAnggota) {
        this.idAnggota = idAnggota;
    }

    public int getJasaUSP() {
        return jasaUSP;
    }

    public void setJasaUSP(int jasaUSP) {
        this.jasaUSP = jasaUSP;
    }

    public int getKblUSP() {
        return kblUSP;
    }

    public void setKblUSP(int kblUSP) {
        this.kblUSP = kblUSP;
    }

    public int getTambahUSP() {
        return tambahUSP;
    }

    public void setTambahUSP(int tambahUSP) {
        this.tambahUSP = tambahUSP;
    }

    public int getCicilanUSP() {
        return cicilanUSP;
    }

    public void setCicilanUSP(int cicilanUSP) {
        this.cicilanUSP = cicilanUSP;
    }

    public int getSisaUSP() {
        return sisaUSP;
    }

    public void setSisaUSP(int sisaUSP) {
        this.sisaUSP = sisaUSP;
    }

    public int getJasaKKH() {
        return jasaKKH;
    }

    public void setJasaKKH(int jasaUKKH) {
        this.jasaKKH = jasaUKKH;
    }

    public int getKblKKH() {
        return kblKKH;
    }

    public void setKblKKH(int kblKKH) {
        this.kblKKH = kblKKH;
    }

    public int getTambahKKH() {
        return tambahKKH;
    }

    public void setTambahKKH(int tambahKKH) {
        this.tambahKKH = tambahKKH;
    }

    public int getCicilanKKH() {
        return cicilanKKH;
    }

    public void setCicilanKKH(int cicilanKKH) {
        this.cicilanKKH = cicilanKKH;
    }

    public int getSisaKKH() {
        return sisaKKH;
    }

    public void setSisaKKH(int sisaKKH) {
        this.sisaKKH = sisaKKH;
    }
   
   
}
